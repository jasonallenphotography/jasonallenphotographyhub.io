##What did you learn about CSS padding, borders, and margin by doing this challenge?
I learned how much easier CSS is for layout than inline modifications within HTML - that would have been much more tedious! In my design, I opted for a liquid layout, and keeping track of total page width was tricky for the multi-column section on the lower half of my page. Because I'm using `float:left`, unless I span all 100% of the page exactly, accounting for padding and margins, weird and undesirable layout issues were happening below the multi-column area. With that said, keeping elements aligned vertically was much simpler using percentages for margins.

##What did you learn about CSS positioning?
I first built the page as a static 960px wide design to roughly place the elements where I wanted them to land. I then began experimenting with liquid layouts, and figuring out where in the style sheet I should put margins to acheive the desired look and feel. One issue I encountered was that I had nested a second `margin: 5%` in my `body` properties, and that caused a cascade of errors in my design, so I learned to start top-down and be careful where I place margin, padding, and border properties.

##What aspects of your design did you find easiest to implement? What was most difficult?
Implementing the nav bar and header were simplest as they had the least complexity. I gave the header section of my site a 5% left and right margin. Adjusting the color of document links when unclicked, clicked, or hovered over was simple as well.

Moving into the floating, liquid 2-column featured text and about-me image, and further the 4-column span below it was tricky. For the featured text/about-me image section, I added a `height: auto;` property to the text block in order to force the horizontal rule below to adjust as it overflowed. Also, I added a `max-width:50%;` `height:auto;` and `width:auto;` property to the featured image, forcing it to scale evenly when the width of the page is reduced in size. I applied a site-global property to `img` for a `max-width: 100%;` which forced images (particularly those in the 4-column part of the layout) to scale to the maximum width of their container if the page was reduced in size.

##What did you learn about adding and formatting elements with CSS in this challenge?
I taught myself a lot of liquid/responsive design elements in the course of this challenge. I plan to further add in a responsive design with CSS for a mobile/small screen platform next.

Building out CSS for a wireframed design is much simpler than designing-as-you-go. Diving into the CSS with a rough or precise idea of where I wanted elements to be and how you want them to flex (or remain static) made life easier than trying to replicate someone else's site's look.